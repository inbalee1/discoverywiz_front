sudo apt-get update && sudo apt-get upgrade

sudo apt-get install apache2

sudo vi /etc/apache2/mods-available/mpm_prefork.conf

sudo a2dismod mpm_event

sudo a2enmod mpm_prefork

sudo systemctl restart apache2

cd /etc/apache2/sites-available

sudo vi discoverywiz.conf

paste this :
<VirtualHost *:80>
     ServerAdmin webmaster@discoverywiz.com
     ServerName discoverywiz
     ServerAlias discoverywiz
     DocumentRoot /var/www/html/discoverywiz/public_html/
     ErrorLog /var/www/html/discoverywiz/logs/error.log
     CustomLog /var/www/html/discoverywiz/logs/access.log combined
</VirtualHost>


sudo mkdir -p /var/www/html/discoverywiz.conf/public_html

sudo mkdir -p /var/www/html/discoverywiz/public_html

sudo mkdir /var/www/html/discoverywiz/logs

sudo a2ensite discoverywiz.conf

sudo systemctl restart apache2

sudo chmod -R 777 /var/www/html

#put front code in public_html folder for now